<div align="center">
   <img src="./assets/app_logo.png width="125"
   height="125"
   style="margin-top: 32px; margin-bottom: 32px;"/>
</div>

# Kittle [Link](https://kittle-7dbab.web.app/#/)
Kittle app is analytical dashboard for cross platform.

# Getting Started
The application is developed with Flutter to create beautiful, fast apps, with a productive, extensible and open development model.

## Install Flutter
* [Install Flutter](https://flutter.dev/get-started/)
* [Flutter documentation](https://flutter.dev/docs)
* [Development wiki](https://github.com/flutter/flutter/wiki)
* [Contributing to Flutter](https://github.com/flutter/flutter/blob/master/CONTRIBUTING.md)

For announcements about new releases and breaking changes, follow the
[flutter-announce@googlegroups.com](https://groups.google.com/forum/#!forum/flutter-announce)
mailing list.


## Flutter Channel
The application is develop in stable flutter channel. You might encounter compile or build failure due to development in wrong channel.
To switch channel

    flutter channel beta
    flutter upgrade
    flutter config --enable-web

## Running on different platforms
    flutter run -d chrome
    flutter run android
    flutter run ios
